﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MainScript : MonoBehaviour {

    //-----VARIABLES-----//
    public static MainScript Instance { get; private set; }

    public GameObject krushka1;
    public GameObject krushka2;
    public GameObject wire;
    public GameObject box001;
    public GameObject box002;
    public GameObject lom;
    public GameObject tube_right;
    public GameObject kaps_left;
    
    //Расположение объектов по порядку
    public List<GameObject> arrInKups = new List<GameObject>();
    public List<GameObject> arrInTube = new List<GameObject>();

	//public List<GameObject> arrInKupsAsnwer1 = new List<GameObject>();
    //public List<GameObject> arrInTubeAsnwer1 = new List<GameObject>();

	//public List<GameObject> arrInKupsAsnwer2 = new List<GameObject>();
    //public List<GameObject> arrInTubeAsnwer2 = new List<GameObject>();

    //Сбоник зон, где могут находиться объекты
    public List<List<GameObject>> areas = new List<List<GameObject>>();

    //-----FUNCTIONS-----//
	void Start () {
        lom.tag = "noMove";
	    krushka1.tag = "noMove";
        krushka2.tag = "noMove";
        wire.tag = "noMove";
        box001.tag = "noMove";
        box002.tag = "noMove";

        //Начальные значение верхней крышки
        krushka1.GetComponent<Rigidbody>().useGravity = false;
        krushka1.GetComponent<Rigidbody>().isKinematic = true;

        arrInKups.Add(box001);
        arrInKups.Add(box002);
        arrInKups.Add(wire);
        arrInKups.Add(krushka2);
        arrInKups.Add(krushka1);

		//arrInKupsAsnwer1 = new List<GameObject>(arrInKups);
		//Debug.Log(arrInKupsAsnwer1);

        areas.Add(arrInKups);
        areas.Add(arrInTube);

        //Instance = this;
	}


    void Update() {}


    public void Awake()
	{
		Instance = this;
	}


    //Вовзращает, входит ли оюъект в область другого объекта
    public bool objectIn(GameObject myObject, GameObject objectArea, float popravka) {
        if ((objectArea.transform.position.x + popravka >= myObject.transform.position.x - objectArea.GetComponent<Renderer>().bounds.size.x/2f) && (objectArea.transform.position.x + popravka <= myObject.transform.position.x + objectArea.GetComponent<Renderer>().bounds.size.x/2f))
        {
            return true;
        }
        else return false;
    }

    
    //Возвращает координаты объекта
    //public Vector3 startCoor(GameObject myObject)
    //{
    //    return myObject.transform.position;
    //}


    public void inNewArea(GameObject gameObject, ref List<GameObject> newArea) {

        if (newArea.Count == 0) {
            newArea.Add(gameObject);
        }
        else {
            if (newArea[newArea.Count - 1] != gameObject)
            {

                foreach (GameObject objectAlone in newArea)
                {
                    objectAlone.tag = "noMove";

                    if ((newArea[newArea.Count - 1] == box001 || newArea[newArea.Count - 1] == box002) && (gameObject == box001 || gameObject == box002))
                    {
                        newArea[newArea.Count - 1].tag = "Move";
                    }
                }
                newArea.Add(gameObject);
            }
        }

        foreach (List<GameObject> area in areas)
        {
            if (area.Contains(gameObject) && area != newArea)
            {
                try
                {
                    area[area.Count - 2].tag = "Move";

                    if ((area[area.Count - 3] == box001 || area[area.Count - 3] == box002) && (area[area.Count - 2] == box001 || area[area.Count - 2] == box002))
                    {
                        area[area.Count - 3].tag = "Move";
                    }
                }
                catch (ArgumentOutOfRangeException) { }

                area.Remove(gameObject);
                break;
            }
        }
    }

    public void notInAnyArea(GameObject gameObject) {
        foreach (List<GameObject> area in areas) {
            if (area.Contains(gameObject)) {
                area.Remove(gameObject);

                if (area.Count != 0)
                {
                    area[area.Count - 1].tag = "Move";

                    try
                    {
                        if ((area[area.Count - 2] == box001 || area[area.Count - 2] == box002) && (area[area.Count - 1] == box001 || area[area.Count - 1] == box002))
                        {
                            area[area.Count - 2].tag = "Move";
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
                break;
            }
        }
    }

    public Vector3 getCoorInNewArea(GameObject gameObject, List<GameObject> newArea, Vector3 place, Vector3 lastCoor, float correction) {

        if (newArea.Count == 0) return place;
        else {
            if (newArea[newArea.Count - 1] == gameObject) {
                return lastCoor;
            }
            
            else {
                return (newArea[newArea.Count - 1]).transform.position + new Vector3(0, correction, 0);
            }
        }
    }


	public List<string> realArrInKups(){
        List<string> answer = new List<string>();

       foreach(GameObject item in this.arrInKups){
            answer.Add(item.gameObject.name);
        }

		return answer;
	}

    public List<string> realArrInTube(){
        List<string> answer = new List<string>();

       foreach(GameObject item in this.arrInTube){
            answer.Add(item.gameObject.name);
        }

		return answer;
	}
}