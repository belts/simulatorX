﻿using System.Collections.Generic;
using UnityEngine;

public class krushka2Script : MonoBehaviour {

    //-----VARIABLES-----//
    private Vector3 placeInKups;
    private Vector3 placeInTube;
    private Vector3 lastCoor;


    //-----FUNCTIONS-----//
	void Start () {
        placeInKups = MainScript.Instance.kaps_left.transform.position + new Vector3(0f, 1.75f, 0f);
        placeInTube = MainScript.Instance.tube_right.transform.position + new Vector3(0, -0.43f,-0.05f);
        lastCoor = this.transform.position;
	}


    void Update() { }


    void OnMouseUp()
    {
        //Если крышка1 находится в зоне тубы
        if (this.gameObject.tag == "noMove") return;
        else
        {

            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.tube_right, -0.5f)) && !Input.anyKeyDown)
            {
                this.putObject(ref MainScript.Instance.arrInTube, placeInTube);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                //*/
                /////////////DEBUG END////////////

                return;
            }



            //Если крышка1 находится в зоне снаряда
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.kaps_left, 0f)) && !Input.anyKeyDown)
            {
                this.putObject(ref MainScript.Instance.arrInKups, placeInKups);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                //*/
                /////////////DEBUG END////////////

                return;
            }
            else
            {
                MainScript.Instance.notInAnyArea(MainScript.Instance.krushka2);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                //*/
                /////////////DEBUG END////////////
            }
        }
    }

    private void putObject(ref List<GameObject> area, Vector3 place)
    {

        if (area.Count == 0) this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka1, area, place, lastCoor, 0f);
        else
        {
            //Над крышка1
            if (area[area.Count - 1] == MainScript.Instance.krushka1)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.1f);
            //проволка --
            if (area[area.Count - 1] == MainScript.Instance.wire)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0f);
            //боксы --
            if (area[area.Count - 1] == MainScript.Instance.box001 || area[area.Count - 1] == MainScript.Instance.box002)
            {
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.14f);

                if (area[area.Count - 1] == MainScript.Instance.box002) this.transform.position += new Vector3(0.15f, 0, 0);
                if (area[area.Count - 1] == MainScript.Instance.box001) this.transform.position += new Vector3(-0.15f, 0, 0);
            }

            //Над крышка2
            if (area[area.Count - 1] == MainScript.Instance.krushka2)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0f);
        }

        lastCoor = this.transform.position;
        MainScript.Instance.inNewArea(MainScript.Instance.krushka2, ref area);
    }
}
