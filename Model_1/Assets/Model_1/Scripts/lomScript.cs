﻿using UnityEngine;

public class lomScript : MonoBehaviour {

    //-----VARIABLES-----//
    private Vector3 startCoor;  //Начальная позиция лома


    //-----FUNCTIONS-----//
	void Start () {
        startCoor = this.transform.position;
	}
   

	void Update () {}


    void OnMouseUp()
    {
        if (MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.kaps_left, 0) && !Input.anyKeyDown)
        {
            this.GetComponent<Animation>().Play();      //Запуск анимации лома
            MainScript.Instance.krushka1.GetComponent<Animation>().Play();  //Запуск анимации верхней крышки
            MainScript.Instance.lom.GetComponent<Rigidbody>().useGravity = true;
            MainScript.Instance.lom.GetComponent<Rigidbody>().isKinematic = false;

            MainScript.Instance.krushka1.GetComponent<Rigidbody>().useGravity = true;
            MainScript.Instance.krushka1.GetComponent<Rigidbody>().isKinematic = false;

            MainScript.Instance.krushka1.tag = "Move";
            MainScript.Instance.krushka2.tag = "Move";

            this.tag = "NoMove";

            MainScript.Instance.arrInKups.Remove(MainScript.Instance.krushka1);
        }
        else {
			this.transform.position = startCoor;
		}
    }
}
