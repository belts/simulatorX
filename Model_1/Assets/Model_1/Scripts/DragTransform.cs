﻿using System.Collections;
using UnityEngine;

class DragTransform : MonoBehaviour
{
    private Transform curObj;
    private string moveTag = "Move";
    //private Color mouseOverColor = Color.blue;
    private float distance;
    private bool isDragging; //Перетягиваю ли я какой-то объект
    private float z;        //Расстояние от камеры до объекта

    Vector3 startCoor;
    //Vector3 lastMouse;      
    Ray ray;    //Луч из камеры к месту нажатия мыши


    //void OnMouseEnter()
    //{
    //    renderer.material.color = mouseOverColor;
    //}

    void OnMouseDown()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);    //Луч из камеры к месту нажатия мыши
        Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.green);
        RaycastHit hit;

        //Возвращаем объект, если наткнулись на него, его можно двигать и мы не двигаем никакой объект в данный момент
        if (Physics.Raycast(ray, out hit)){
            if (GetTag(hit.transform.tag) && (curObj == null)){

                curObj = hit.transform;
                isDragging = true;
                distance = 2.1f;
                z = curObj.transform.position.z;

                startCoor = curObj.transform.position;

            }
        }
    }

    void OnMouseUp()
    {
        isDragging = false;

        
        //curObj.transform.position = startCoor;

        //curObj.transform.position = new Vector3(transform.position.x, transform.position.y, z);
        curObj = null;
    }

    void Update()
    {
        //Если можно перетягивать и если я не перетягиваю объект
        if (isDragging)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            curObj.transform.position = rayPoint;
        }
    }

    public bool GetTag(string curTag)
    {
        //Сравниваем тег объекта с тегом "move"
        if (curTag == moveTag) return true; 
        else return false;
    }
}

//distance = Vector3.Distance(transform.position - new Vector3(0,0,0.5f), Camera.main.transform.position);