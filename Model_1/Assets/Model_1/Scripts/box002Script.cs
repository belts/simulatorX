﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class box002Script : MonoBehaviour {

    //-----VARIABLES-----//
    private Vector3 placeInKups;
    private Vector3 placeInTube;
    private Vector3 lastCoor;


    //-----FUNCTIONS-----//

	void Start () {
        placeInKups = MainScript.Instance.kaps_left.transform.position + new Vector3(-0.16f, 1.88f, 0f);
        placeInTube = MainScript.Instance.tube_right.transform.position + new Vector3(-0.16f, -0.3f,-0.05f);
        lastCoor = this.transform.position;
	}
	

	void Update () {}


    void OnMouseUp()
    {
        //Если box002 находится в зоне тубы
        if (this.gameObject.tag == "noMove") return;
        else
        {
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.tube_right, -0.5f)) && !Input.anyKeyDown)
            {
                this.putObject(ref MainScript.Instance.arrInTube, placeInTube);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                */
                /////////////DEBUG END////////////

                return;
            }



            //Если box002 находится в зоне снаряда
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.kaps_left, 0f)) && !Input.anyKeyDown)
            {
                this.putObject(ref MainScript.Instance.arrInKups, placeInKups);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                */
                /////////////DEBUG END////////////

                return;
            }
            else
            {
                MainScript.Instance.notInAnyArea(MainScript.Instance.box002);

                /////////////DEBUG START////////////
                /*
                foreach (GameObject objectAlone in MainScript.Instance.arrInKups)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }

                foreach (GameObject objectAlone in MainScript.Instance.arrInTube)
                {
                    //foreach (GameObject objectAlone in objectsMass)
                    //{
                        Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
                    //}
                }
                Debug.Log(" ");
                */
                /////////////DEBUG END////////////
            }
        }
    }

    private void putObject(ref List<GameObject> area, Vector3 place)
    {
        if (area.Count == 0) this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0f);
        else
        {
            //Над крышка1
            if (area[area.Count - 1] == MainScript.Instance.krushka1)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0.35f);
            //Над крышка2
            if (area[area.Count - 1] == MainScript.Instance.krushka2)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0.3f);
            //Над wire
            if (area[area.Count - 1] == MainScript.Instance.wire)
            {
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0.2f);
            }
            //Над box1
            if (area[area.Count - 1] == MainScript.Instance.box001)
            {
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0f);
                this.transform.position += new Vector3(-0.15f, 0, 0);
            }

            this.transform.position += new Vector3(-0.15f, 0, 0);

            //Над box2
            if (area[area.Count - 1] == MainScript.Instance.box002)
            {
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.box002, area, place, lastCoor, 0f);
            }
        }

        lastCoor = this.transform.position;
        MainScript.Instance.inNewArea(MainScript.Instance.box002, ref area);
    }
}
