﻿using UnityEngine;
using System.Collections.Generic;

public class Services : MonoBehaviour{
    public static Services Instance { get; private set; }

    void Awake(){
        this.initSimulators();
        this.choisenSimulator = this.chooseRandomSimulator();

        Instance = this; 
    }

    private List<Simulator> simulators;
    private Simulator choisenSimulator = null;

    private void initSimulators(){
        this.simulators = new List<Simulator>();

        this.simulators.Add(new Simulator("Тренажер заряду ЖН-546",
                                          "Зібрати заряд ЖН-546 \"Повний\"",
                                           new List<string>(){"box001",
                                                              "box002",
                                                              "wire",
                                                              "krushka2"},
                                           new List<string>(){"krushka1"},
                                           new string[]{"Артилерійське озброєння і боєприпаси. П. 5.4 Поводження з боєприпасами на вогневій позиції та підготовка їх до стрільби."}));

        this.simulators.Add(new Simulator("Тренажер заряду ЖН-546",
                                          "Зібрати заряд ЖН-546 \"Перший\"",
                                          new List<string>(){"wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1",
                                                             "box001",
                                                             "box002"},
                                          new string[]{"Артилерійське озброєння і боєприпаси. П. 5.4 Поводження з боєприпасами на вогневій позиції та підготовка їх до стрільби."}));
    }

    private Simulator chooseRandomSimulator(){
        return this.simulators[Random.Range(0, this.simulators.Count)];
    }

    public Simulator getChoisenSimulator(){
        return this.choisenSimulator;
    }

    public List<string> getRealArrInKups(){
        return MainScript.Instance.realArrInKups();
    }

    public List<string> getRealArrInTube(){
        return MainScript.Instance.realArrInTube();
    }
}
