using UnityEngine;
using System.Collections.Generic;
using System;

public class Simulator{

    public Simulator(string taskTitle, string task,
                    List<string> rightArrInKups, List<string> rightArrInTube){
        
        this.taskTitle = taskTitle;
        this.task = task;
        this.rightArrInKups = new List<string>(rightArrInKups);
        this.rightArrInTube = new List<string>(rightArrInTube);
    }

    public Simulator(string taskTitle, string task,
                    List<string> rightArrInKups, List<string> rightArrInTube,
                    string[] helpfulLinks){
        
        this.taskTitle = taskTitle;
        this.task = task;
        
        this.rightArrInKups = new List<string>(rightArrInKups);
        this.rightArrInTube = new List<string>(rightArrInTube);

        this.helpfulLinks = new string[helpfulLinks.Length];
        Array.Copy(helpfulLinks, this.helpfulLinks, helpfulLinks.Length);
    }

    private string taskTitle;
    private string task;
    private List<string> rightArrInKups;
    private List<string> rightArrInTube;
    internal string[] helpfulLinks{ get; private set; }


    public string getTaskTitle()
    {
        return this.taskTitle;
    }

    public string getTask()
    {
        return this.task;
    }

    public List<string> getKupsAnswer(){
        return this.rightArrInKups;
    }

    public List<string> getTubeAnswer(){
        return this.rightArrInTube;
    }
}