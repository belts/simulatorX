﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startTest : MonoBehaviour {

    // VARIABLES //
    public GameObject startTestButton;
    public GameObject endTestButton;
    public GameObject outButton;
    
    public GameObject panelStart;
    public GameObject panelWork;
    public GameObject outPanel;
    
    public UnityEngine.UI.Text taskTitile;
    public UnityEngine.UI.Text taskText;
    public UnityEngine.UI.Image taskImage;
    
    public UnityEngine.UI.Text timerText;

    public UnityEngine.UI.Text taskTitileEndPanel;    
    public UnityEngine.UI.Text taskTextEndPanel;
    public UnityEngine.UI.Text timeText;

    public UnityEngine.UI.Text mistakeText;
    public UnityEngine.UI.Text markText;

    public UnityEngine.UI.Text helpText;

    private float stopwatch = 0;
    private bool isStopwatchActive = false;
    private float mark;
    private int amountOfMistake;


    // FUNCTIONS //
    void Start(){
        UnityEngine.Debug.Log("UI: IN Start");

        this.taskText.text = Services.Instance.getChoisenSimulator().getTask();
        this.taskTitile.text = Services.Instance.getChoisenSimulator().getTaskTitle();

        this.panelStart.SetActive(true);
    }

    void Update(){
        if(this.isStopwatchActive){
            this.stopwatch += 1*Time.deltaTime;
            this.timerText.text = this.getTimeTestString();
        }
    }

    public void startTestButtonPress(){
        UnityEngine.Debug.Log("UI: button startTest was been pressed.");
        
        panelStart.gameObject.SetActive(false);
        panelWork.gameObject.SetActive(true);

        MainScript.Instance.lom.tag = "Move";

        this.startTimeTest();
    }

    public void endTestPress(){
        Debug.Log("UI: button endTest was been pressed.");
        this.taskTextEndPanel.text = Services.Instance.getChoisenSimulator().getTask();
        this.taskTitileEndPanel.text = Services.Instance.getChoisenSimulator().getTaskTitle();
        this.panelWork.gameObject.SetActive(false);
        this.outPanel.gameObject.SetActive(true);

        this.endTimeTest();

        this.mistakeText.text = this.amountOfMistake.ToString();

        this.timeText.text = this.getTimeTestString();

        this.mark = this.calculateMark();
        this.markText.text = this.mark.ToString();
        
        // This line should be after calling function calculateMark()
        // because in this fucntion set value to variable amountOfMistake
        this.mistakeText.text = this.amountOfMistake.ToString();

        this.helpText.text = "";
        
        foreach(string item in Services.Instance.getChoisenSimulator().helpfulLinks){
            this.helpText.text += item + "\n";
        }
        
        switch((int)this.mark){
            case 5:
                this.markText.color = UnityEngine.Color.green;
                break; 
            case 4:
                break;
            case 3:
                this.markText.color = UnityEngine.Color.yellow;
                break;
            case 2:
            case 1:
            case 0:
                this.markText.color = UnityEngine.Color.red;
                break;
            default:
                Debug.Log("startTest.endTestPress(): ERROR: Unknown value of mark!");
                break;
        }
    }

    public void outTestPress()
    {
        Debug.Log("UI: button outTest was been pressed.");
        Application.Quit();
    }

    public void startTimeTest(){
        this.isStopwatchActive = true;
    }

    public void endTimeTest(){
        this.isStopwatchActive = false;
    }

    private int minutes = 0;
    private int seconds = 0;
    public string getTimeTestString(){
        string time = "";
        minutes = ((int)this.stopwatch) / 60;
        seconds = ((int)this.stopwatch) % 60;

        if(minutes<10) time += "0";
        time += minutes.ToString() + ":";
        
        if(seconds<10) time += "0";
        time += seconds.ToString();
        
        return time;
    }

    private float calculateMark(){
        int amountOfMistake = 0;

        List<string> realArrInKupsList = Services.Instance.getRealArrInKups();
        List<string> realArrInTubeList = Services.Instance.getRealArrInTube();

        List<string> rightArrInKupsList = Services.Instance.getChoisenSimulator().getKupsAnswer();
        List<string> rightArrInTubeList = Services.Instance.getChoisenSimulator().getTubeAnswer();

        string[] realArrInKupsArray = realArrInKupsList.ToArray();
        string[] realArrInTubeArray = realArrInTubeList.ToArray();

        string[] rightArrInKupsArray = rightArrInKupsList.ToArray();
        string[] rightArrInTubeArray = rightArrInTubeList.ToArray();

        Debug.Log("Check arr in kups:");
        foreach(string rightItem in rightArrInKupsArray){
            Debug.Log(rightItem);
            Debug.Log("real:" + Array.IndexOf(realArrInKupsArray, rightItem));
            Debug.Log("right:" + Array.IndexOf(rightArrInKupsArray, rightItem));
            

            if(Array.IndexOf(realArrInKupsArray, rightItem) != Array.IndexOf(rightArrInKupsArray, rightItem)){
                amountOfMistake++;
            }
        }

        Debug.Log("Check arr in tube:");
        foreach(string rightItem in rightArrInTubeArray){
            Debug.Log(rightItem);
            Debug.Log("real:" + Array.IndexOf(realArrInTubeArray, rightItem));
            Debug.Log("right:" + Array.IndexOf(rightArrInTubeArray, rightItem));

            if(Array.IndexOf(realArrInTubeArray, rightItem) != Array.IndexOf(rightArrInTubeArray, rightItem)){
                amountOfMistake++;
            }
        }

        Debug.Log("Amount of mistakes:" + amountOfMistake);
        
        float mark = 0;

        switch(amountOfMistake){
            case 0:
                mark = 5;
                break;
            case 1:
            case 2:
                mark = 4;
                break;
            case 3:
            case 4:
                mark = 3;
                break;
            default:
                mark = 2;
                break;
        }

        this.amountOfMistake = amountOfMistake;

        Debug.Log("Calculated mark considering errors: " + mark);

        if((int)stopwatch / 60.0f < 5);
        else
            if((int)stopwatch / 60.0f > 5 && (int)stopwatch / 60.0f < 6) mark -= 0.5f;
            else
                if((int)stopwatch / 60.0f > 6 && (int)stopwatch / 60.0f < 7) mark -= 1.0f;
                else
                    if((int)stopwatch / 60.0f > 7 && (int)stopwatch / 60.0f < 8) mark -= 1.5f;
                        else mark = 0;

        Debug.Log("Calculated mark considering errors and time: " + mark);

        return mark;
    }
}