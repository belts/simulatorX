﻿using UnityEngine;
using System.Collections.Generic;

public class puchok4Script : MonoBehaviour {

    //-----VARIABLES-----//
    private Vector3 placeInKups;
    private Vector3 placeInTube;
    private Vector3 lastCoor;


    //-----FUNCTIONS-----//
	void Start () {
        placeInKups = MainScript.Instance.kaps_left.transform.position + new Vector3(0f, 1f, 0f);
        placeInTube = MainScript.Instance.tube_right.transform.position + new Vector3(0, -0.28f,-0.05f);
        lastCoor = this.transform.position;
	}
	
    void OnMouseUp(){
        if (this.gameObject.tag == "noMove") return;
        else{
			//If this object in area of tube
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.tube_right, -0.5f))
					&& !Input.anyKeyDown){

                this.putObject(ref MainScript.Instance.arrInTube, placeInTube);

                MainScript.Instance.printObjectsInAreas();

                return;
            }


            //If this object in area of kups
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.kaps_left, 0f)) 
					&& !Input.anyKeyDown){

                this.putObject(ref MainScript.Instance.arrInKups, placeInKups);

                MainScript.Instance.printObjectsInAreas();

                return;
            }
            else{
                MainScript.Instance.notInAnyArea(MainScript.Instance.puchok4);

                MainScript.Instance.printObjectsInAreas();

				return;
            }
        }
    }

    private void putObject(ref List<GameObject> area, Vector3 place){
        if (area.Count == 0)
			this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0);
        else{
            //Over krushka1
            if (area[area.Count - 1] == MainScript.Instance.krushka1)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.20f);
            
			//Over krushka2
            if (area[area.Count - 1] == MainScript.Instance.krushka2)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.20f);

			//Over puchok5
            if (area[area.Count - 1] == MainScript.Instance.puchok5)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.21f);

            //Over puchok3
            if (area[area.Count - 1] == MainScript.Instance.puchok3)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.21f);

            //Over puchok2
            if (area[area.Count - 1] == MainScript.Instance.puchok2)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.21f);

            //Over puchok1
            if (area[area.Count - 1] == MainScript.Instance.puchok1)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.21f);

            //Over wire
            if (area[area.Count - 1] == MainScript.Instance.wire)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0.13f);

			//Put in the same area
            if (area[area.Count - 1] == MainScript.Instance.puchok4)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.puchok4, area, place, lastCoor, 0);
        }

        lastCoor = this.transform.position;
        MainScript.Instance.inNewArea(MainScript.Instance.puchok4, ref area);
    }
}