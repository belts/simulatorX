﻿using UnityEngine;
using System.Collections.Generic;

public class Services : MonoBehaviour{
    public static Services Instance { get; private set; }

    void Awake(){
        this.initSimulators();
        this.choisenSimulator = this.chooseRandomSimulator();

        Instance = this;
    }

    private List<Simulator> simulators;
    private Simulator choisenSimulator = null;

    private void initSimulators(){
        this.simulators = new List<Simulator>();

        this.simulators.Add(new Simulator("Тренажер заряда ЖН-546У",
                                          "Зібрати заряд ЖН-546У \"Другий\"",
                                          new List<string>(){"puchok1",
                                                             "puchok2",
                                                             "puchok3",
                                                             "puchok4",
                                                             "puchok5",
                                                             "wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1"},
                                          new string[]{"Посилання 21",
                                                       "Посилання 22"}));

        this.simulators.Add(new Simulator("Тренажер заряда ЖН-546У",
                                          "Зібрати заряд ЖН-546У \"Третiй\"",
                                          new List<string>(){"puchok1",
                                                             "puchok2",
                                                             "puchok3",      
                                                             "wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1",
                                                             "puchok5",
                                                             "puchok4"},
                                          new string[]{"Посилання 31",
                                                       "Посилання 32"}));

        this.simulators.Add(new Simulator("Тренажер заряда ЖН-546У",
                                          "Зібрати заряд ЖН-546У \"Четвертий\"",
                                          new List<string>(){"puchok1",
                                                             "puchok2",
                                                             "wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1",
                                                             "puchok5",
                                                             "puchok4",
                                                             "puchok3"},
                                          new string[]{"Посилання 41",
                                                       "Посилання 42"}));

        this.simulators.Add(new Simulator("Тренажер заряда ЖН-546У",
                                          "Зібрати заряд ЖН-546У \"П\'ятий\"",
                                          new List<string>(){"puchok1",
                                                             "wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1",
                                                             "puchok5",
                                                             "puchok4",
                                                             "puchok3",
                                                             "puchok2"},
                                          new string[]{"Посилання 51",
                                                       "Посилання 52"}));

        this.simulators.Add(new Simulator("Тренажер заряда ЖН-546У",
                                          "Зібрати заряд ЖН-546У \"Шостий\"",
                                          new List<string>(){"wire",
                                                             "krushka2"},
                                          new List<string>(){"krushka1",
                                                             "puchok5",
                                                             "puchok4",
                                                             "puchok3",
                                                             "puchok2",
                                                             "puchok1"},
                                          new string[]{"Посилання 61",
                                                       "Посилання 62"}));

    }


    private Simulator chooseRandomSimulator(){
        return this.simulators[Random.Range(0, this.simulators.Count)];
    }

    public Simulator getChoisenSimulator(){
        return this.choisenSimulator;
    }

    public List<string> getRealArrInKups(){
        return MainScript.Instance.realArrInKups();
    }

    public List<string> getRealArrInTube(){
        return MainScript.Instance.realArrInTube();
    }
}
