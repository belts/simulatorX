﻿using System.Collections.Generic;
using UnityEngine;

public class krushka2Script : MonoBehaviour {

    //-----VARIABLES-----//
    private Vector3 placeInKups;
    private Vector3 placeInTube;
    private Vector3 lastCoor;


    //-----FUNCTIONS-----//
	void Start () {
        placeInKups = MainScript.Instance.kaps_left.transform.position + new Vector3(0f, 2.05f, 0f);
        placeInTube = MainScript.Instance.tube_right.transform.position + new Vector3(0, -0.4f,-0.05f);
        lastCoor = this.transform.position;
	}

    void OnMouseUp(){
        if (this.gameObject.tag == "noMove") return;
        else{
            //If this object in area of tube
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.tube_right, -0.5f))
                    && !Input.anyKeyDown){

                this.putObject(ref MainScript.Instance.arrInTube, placeInTube);

                MainScript.Instance.printObjectsInAreas();

                return;
            }

            //If this object in area of kups
            if ((MainScript.Instance.objectIn(this.gameObject, MainScript.Instance.kaps_left, 0f))
                    && !Input.anyKeyDown){

                this.putObject(ref MainScript.Instance.arrInKups, placeInKups);

                MainScript.Instance.printObjectsInAreas();

                return;
            }
            else{
                MainScript.Instance.notInAnyArea(MainScript.Instance.krushka2);

                MainScript.Instance.printObjectsInAreas();

                return;
            }
        }
    }

    private void putObject(ref List<GameObject> area, Vector3 place){
        if (area.Count == 0) this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka1, area, place, lastCoor, 0f);
        else{
            //Over krushka1
            if (area[area.Count - 1] == MainScript.Instance.krushka1)
                this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.1f);
            else{
                if (area == MainScript.Instance.arrInKups) 
                    this.transform.position = placeInKups;
                else{
                    //Over wire
                    if (area[area.Count - 1] == MainScript.Instance.wire)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0f);

                    //Over puchok5
                    if (area[area.Count - 1] == MainScript.Instance.puchok5)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.11f);

                    //Over puchok4
                    if (area[area.Count - 1] == MainScript.Instance.puchok4)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.11f);

                    //Over puchok3
                    if (area[area.Count - 1] == MainScript.Instance.puchok3)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.11f);

                    //Over puchok2
                    if (area[area.Count - 1] == MainScript.Instance.puchok2)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.11f);

                    //Over puchok1
                    if (area[area.Count - 1] == MainScript.Instance.puchok1)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0.11f);

                    //Put in the same area
                    if (area[area.Count - 1] == MainScript.Instance.krushka2)
                        this.transform.position = MainScript.Instance.getCoorInNewArea(MainScript.Instance.krushka2, area, place, lastCoor, 0f);
                }
            }
        }

        lastCoor = this.transform.position;
        MainScript.Instance.inNewArea(MainScript.Instance.krushka2, ref area);
    }
}