﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MainScript : MonoBehaviour {

    //-----VARIABLES-----//
    public static MainScript Instance { get; private set; }

    public GameObject krushka1;
    public GameObject krushka2;
    public GameObject wire;
    public GameObject puchok5;
    public GameObject puchok4;
    public GameObject puchok3;
    public GameObject puchok2;
    public GameObject puchok1;

    public GameObject lom;
    public GameObject tube_right;
    public GameObject kaps_left;
    
    //Areas. In this lists will be objects
    public List<GameObject> arrInKups = new List<GameObject>();
    public List<GameObject> arrInTube = new List<GameObject>();

    // All possible zones
    public List<List<GameObject>> areas = new List<List<GameObject>>();


    //-----FUNCTIONS-----//
	void Start () {
	    krushka1.tag = "noMove";
        krushka2.tag = "noMove";
        wire.tag = "noMove";
        puchok5.tag = "noMove";
        puchok4.tag = "noMove";
        puchok3.tag = "noMove";
        puchok2.tag = "noMove";
        puchok1.tag = "noMove";

        lom.tag = "noMove";

        //Initial value of the krushka1
        krushka1.GetComponent<Rigidbody>().useGravity = false;
        krushka1.GetComponent<Rigidbody>().isKinematic = true;

        arrInKups.Add(puchok1);
        arrInKups.Add(puchok2);
        arrInKups.Add(puchok3);
        arrInKups.Add(puchok4);
        arrInKups.Add(puchok5);
        arrInKups.Add(wire);
        arrInKups.Add(krushka2);
        arrInKups.Add(krushka1);

        areas.Add(arrInKups);
        areas.Add(arrInTube);
	}

    public void Awake(){
		Instance = this;
	}


    // Retrun true if object is in the area of another object
    public bool objectIn(GameObject myObject, GameObject objectArea, float popravka) {
        if ((objectArea.transform.position.x + popravka >=
                myObject.transform.position.x - objectArea.GetComponent<Renderer>().bounds.size.x/2f)
                && (objectArea.transform.position.x + popravka <=
                        myObject.transform.position.x + objectArea.GetComponent<Renderer>().bounds.size.x/2f)){
            
            return true;
        }
        else{
            return false;
        }
    }

    public void inNewArea(GameObject gameObject, ref List<GameObject> newArea) {

        if (newArea.Count == 0) {
            newArea.Add(gameObject);
        }
        else {
            if (newArea[newArea.Count - 1] != gameObject){
                foreach (GameObject objectAlone in newArea){
                    objectAlone.tag = "noMove";
                }
                newArea.Add(gameObject);
            }
        }

        foreach (List<GameObject> area in areas){
            if (area.Contains(gameObject) && area != newArea){
                try{
                    area[area.Count - 2].tag = "Move";
                }catch (ArgumentOutOfRangeException) { }

                area.Remove(gameObject);
                break;
            }
        }
    }

    public void notInAnyArea(GameObject gameObject) {
        foreach (List<GameObject> area in areas) {
            if (area.Contains(gameObject)) {
                area.Remove(gameObject);

                if (area.Count != 0){
                    area[area.Count - 1].tag = "Move";
                }
                break;
            }
        }
    }

    public Vector3 getCoorInNewArea(GameObject gameObject, List<GameObject> newArea,
                                    Vector3 place, Vector3 lastCoor,
                                    float correction) {

        if (newArea.Count == 0) return place;
        else {
            if (newArea[newArea.Count - 1] == gameObject) {
                return lastCoor;
            }
            
            else {
                return (newArea[newArea.Count - 1]).transform.position + new Vector3(0, correction, 0);
            }
        }
    }

    public void printObjectsInAreas(){
        foreach (GameObject objectAlone in this.arrInKups){
            Debug.Log("arrInKups: " + objectAlone.name + " - " + objectAlone.tag);
        }

        foreach (GameObject objectAlone in this.arrInTube){
            Debug.Log("arrInTube: " + objectAlone.name + " - " + objectAlone.tag);
        }
        
        Debug.Log(" ");
    }

    public List<string> realArrInKups(){
        List<string> answer = new List<string>();

       foreach(GameObject item in this.arrInKups){
            answer.Add(item.gameObject.name);
        }

		return answer;
	}

    public List<string> realArrInTube(){
        List<string> answer = new List<string>();

       foreach(GameObject item in this.arrInTube){
            answer.Add(item.gameObject.name);
        }

		return answer;
	}
}